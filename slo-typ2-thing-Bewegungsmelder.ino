// Bewegungsmelder über Thingspeak und wifimanagereingabemodul
//entwickelt von SLO siehe eBook https://sloiot.pressbooks.com/ unter Nutzung von 
 //https://github.com/tzapu/WiFiManager 
#include <FS.h>                   //this needs to be first, or it all crashes and burns...

#include <ESP8266WiFi.h>          //https://github.com/esp8266/Arduino

//needed for library


#include <WiFiManager.h>          //https://github.com/tzapu/WiFiManager

#include <ArduinoJson.h>          //https://github.com/bblanchon/ArduinoJson
  #include "SPI.h"

#include <OneWire.h>

#include <DNSServer.h>
#include <ESP8266WebServer.h>  
#include "ThingSpeak.h"
  
  int status = WL_IDLE_STATUS;
  WiFiClient  client;
  // On ESP8266:  0 - 1023 maps to 0 - 1 volts
  #define VOLTAGE_MAX 3.3
  #define VOLTAGE_MAXCOUNTS 1023.0
  #define myPeriodic 15 //in sec | Thingspeak pub is 15sec

String feldnummer="5";// Eingabe der Feldnummer
String WriteAPIKey1 ="999999999999";// Eingabe des WriteAPI-Key hier geändert


//String myChannelNumber="99999";

const char* server = "api.thingspeak.com";
float prevTemp = 0;
int sent = 0;
// Ende der Eingabe
//define your default values here, if there are different values in config.json, they are overwritten.

char apiKey_token[34] = "api Key";
char FeldID_token[34] = "";


char ssid[] = "";
char pass[] = "";
char apiKey[21] = "";
char FeldID[30] = "";
//flag for saving data
bool shouldSaveConfig = false;

//callback notifying us of the need to save config
void saveConfigCallback () {
  Serial.println("Should save config");
  shouldSaveConfig = true;
}



void setup() {
  // put your setup code here, to run once:
  WiFi.disconnect(true);  
  Serial.begin(115200);// geändert von 9600
  Serial.println();

  //clean FS, for testing
 SPIFFS.format();

  //read configuration from FS json
  Serial.println("mounting FS...line 60");

 if (SPIFFS.begin()) {
  Serial.println("mounted file system line 63");
    if (SPIFFS.exists("/config.json")) {
     //file exists, reading and loading
     Serial.println("reading config file");
      File configFile = SPIFFS.open("/config.json", "r");
      if (configFile) {
        Serial.println("opened config file");
        size_t size = configFile.size();
        // Allocate a buffer to store contents of the file.
       std::unique_ptr<char[]> buf(new char[size]);

        configFile.readBytes(buf.get(), size);
        DynamicJsonBuffer jsonBuffer;
        JsonObject& json = jsonBuffer.parseObject(buf.get());
        json.printTo(Serial);
        if (json.success()) {
          Serial.println("\nparsed json");

 
          strcpy(apiKey,  json["apiKey"]);
        strcpy(FeldID, json["Feldnummer"]);

        } else {
          Serial.println("failed to load json config line 86");
        }
       configFile.close();
      }
    }
  } else {
    Serial.println("failed to mount FS line 91");
  }
  //end read



  // The extra parameters to be configured (can be either global or just in the setup)
  // After connecting, parameter.getValue() will get you the configured value
  // id/name placeholder/prompt default length
 
  WiFiManagerParameter customApiKey("apiKEY", "API Key", apiKey, 50);
  WiFiManagerParameter custom_FeldID_token("FeldID", "Feldnummer", FeldID_token, 10);
  //WiFiManager
  //Local intialization. Once its business is done, there is no need to keep it around
  WiFiManager wifiManager;
Serial.println(" line 119");
  //set config save notify callback
  wifiManager.setSaveConfigCallback(saveConfigCallback);

 
  
  //add all your parameters here
 
  wifiManager.addParameter(&customApiKey);
  wifiManager.addParameter(&custom_FeldID_token);
 Serial.println(" line 132"); 
  //reset settings - for testing
//wifiManager.resetSettings();
 Serial.println(" line 135");
  //set minimu quality of signal so it ignores AP's under that quality
  //defaults to 8%
  //wifiManager.setMinimumSignalQuality();
  
  //sets timeout until configuration portal gets turned off
  //useful to make it all retry or go to sleep
  //in seconds
wifiManager.setTimeout(120);

  //fetches ssid and pass and tries to connect
  //if it does not connect it starts an access point with the specified name
  //here  "AutoConnectAP"
  //and goes into a blocking loop awaiting configuration
 
  Serial.println("ssid und pwd line 146");
  
  if (!wifiManager.autoConnect()) {
    Serial.println("failed to connect and hit timeout");
    delay(3000);
  //  reset and try again, or maybe put it to deep sleep
  // ESP.reset();
    delay(5000);
  }

  //if you get here you have connected to the WiFi
  Serial.println("connected...yeey :)");
   

  //read updated parameters
//Fehlermeldung bei 

  strcpy(apiKey, customApiKey.getValue());  
 strcpy(FeldID, custom_FeldID_token.getValue());
 //
 //
///save the custom parameters to FS
  if (shouldSaveConfig) {
    Serial.println("saving config line 167");
    DynamicJsonBuffer jsonBuffer;
    JsonObject& json = jsonBuffer.createObject();

    json["apikEY"] = apiKey;
    json["FeldID_token"] = FeldID;

    File configFile = SPIFFS.open("/config.json", "w");
    if (!configFile) {
      Serial.println("failed to open config file for writing");
    }

    json.printTo(Serial);
    json.printTo(configFile);
    configFile.close();
    //end save
   
 //     ThingSpeak.begin(client);
        Serial.println(" line 195");
}
  
ThingSpeak.begin(client);
  Serial.println(" line 198");
 Serial.println(apiKey);
 Serial.println(FeldID);
 
 WriteAPIKey1=apiKey;
 feldnummer=FeldID;
  Serial.println( WriteAPIKey1);



}

void loop() {
  // read the input on analog pin 0:
  int sensorValue = analogRead(A0);
  // Convert the analog reading 
  // On Arduino:  0 - 1023 maps to 0 - 5 volts
  // On ESP8266:  0 - 1023 maps to 0 - 1 volts
  // On Particle: 0 - 4095 maps to 0 - 3.3 volts
  float voltage = sensorValue * (VOLTAGE_MAX / VOLTAGE_MAXCOUNTS);
  Serial.println(voltage);
   Serial.println(apiKey);
   Serial.println(FeldID);
  
 

//ab hier Daten eingefügt aus dem anderen Temperatursketch //void sendTeperatureTS(float temp) {  
   
   Serial.println(server);// test eingefügt
   WiFiClient client;
  Serial.println(server);// test eingefügt
   if (client.connect(server, 80)) { // use ip 184.106.153.149 or api.thingspeak.com
   Serial.println("WiFi Client connected  line 228");
  WriteAPIKey1 =apiKey;
   Serial.println(WriteAPIKey1);
   // eingefügt für tests
   Serial.println(apiKey);//
 
    String postStr = apiKey;

    postStr += "&field";
    postStr += String(feldnummer);
    postStr += "=";
    postStr += String(voltage);
    postStr += "\r\n\r\n";

  
  client.print("POST /update HTTP/1.1\n");
  client.print("Host: api.thingspeak.com\n");

  client.print("Connection: close\n");
  client.print("X-THINGSPEAKAPIKEY: " + WriteAPIKey1 + "\n");
 
 client.print("Content-Type: application/x-www-form-urlencoded\n");
   client.print("Content-Length: ");
   client.print(postStr.length());
   client.print("\n\n");
   client.print(postStr);
   delay(1000);
   
   }//end if
   sent++;
 client.stop();
}//end send


