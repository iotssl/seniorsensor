/***slo-typ2-bewegung-v2
Als Vorlage wurde der Sketch aus http://www.instructables.com/id/Blynk-Arduino-DS18B20-Thermometer-Display-on-IOS-o/?ALLSTEPS
benutzt und für den NodeMCU 1.0 und HC-SR 501 über Blynk von @hosi1709, SLO umgeändert
02.2018 geändert Vorschlag https://community.blynk.cc/t/blynk-app-to-set-esp8266-12e-auth-ssid-and-pass/9017/19***/
#include <FS.h>                   //this needs to be first, or it all crashes and burns...          
#include <SPI.h>
#include <ESP8266WiFi.h>
#include <BlynkSimpleEsp8266.h>
#include <OneWire.h>
#include <DNSServer.h>
#include <ESP8266WebServer.h>
#include <WiFiManager.h>          //https://github.com/tzapu/WiFiManager
#include <ArduinoJson.h>          //https://github.com/bblanchon/ArduinoJson
#define BLYNK_PRINT Serial // startet den Serial Monitor



String auth1="8120e7c56fab445f9f6ac2fedeab3434";
//char auth[] = "8120e7c56fab445f9f6ac2fedeab3434";   // Name des Auth Token 
char ssid[] = "WLAN-177223";    //WlAN Name
char pass[] = "9247669398923971"; //WLAN Passwort

int sensorData;
BlynkTimer timer;

void myTimerEvent()
{
  sensorData = analogRead(0);         //Outputpin Sensor auf Pin A0 am NodeMCU
  Blynk.virtualWrite(V4, sensorData); //Widgets in der App Blynk auf Pin V4 und Push
}
//slo


 char blynk_token[33] = "YOUR_BLYNK_TOKEN";


char auth[] = "8120e7c56fab445f9f6ac2fedeab3434";

 //  Serial.println("line46");
//flag for saving data
bool shouldSaveConfig = false;

//callback notifying us of the need to save config
void saveConfigCallback () {
  Serial.println("Should save config");
  shouldSaveConfig = true;
}


void setup() {
  // put your setup code here, to run once:
 
  Serial.begin(115200);
  Serial.println();
  Serial.println("line70");
  //clean FS, for testing
 // SPIFFS.format();

  //read configuration from FS json
  Serial.println("mounting FS...");

  if (SPIFFS.begin()) {
    Serial.println("mounted file system");
    if (SPIFFS.exists("/config.json")) {
      //file exists, reading and loading
      Serial.println("reading config file");
      File configFile = SPIFFS.open("/config.json", "r");
      if (configFile) {
        Serial.println("opened config file");
        size_t size = configFile.size();
        // Allocate a buffer to store contents of the file.
        std::unique_ptr<char[]> buf(new char[size]);

        configFile.readBytes(buf.get(), size);
        DynamicJsonBuffer jsonBuffer;
        JsonObject& json = jsonBuffer.parseObject(buf.get());
        json.printTo(Serial);
        if (json.success()) {
          Serial.println("\nparsed json");

        //  strcpy(mqtt_server, json["mqtt_server"]);
        //  strcpy(mqtt_port, json["mqtt_port"]);
          strcpy(blynk_token, json["blynk_token"]);

        } else {
          Serial.println("failed to load json config");
        }
 //       configFile.close();
      }
    }
  } else {
    Serial.println("failed to mount FS");
  }
  //end read
  Serial.println("line110");


  // The extra parameters to be configured (can be either global or just in the setup)
  // After connecting, parameter.getValue() will get you the configured value
  // id/name placeholder/prompt default length
 // WiFiManagerParameter custom_mqtt_server("server", "mqtt server", mqtt_server, 40);
 // WiFiManagerParameter custom_mqtt_port("port", "mqtt port", mqtt_port, 6);
  WiFiManagerParameter custom_blynk_token("blynk", "blynk token", blynk_token, 35);

  //WiFiManager
  //Local intialization. Once its business is done, there is no need to keep it around
  WiFiManager wifiManager;

  //set config save notify callback
  wifiManager.setSaveConfigCallback(saveConfigCallback);

  //set static ip
//  wifiManager.setSTAStaticIPConfig(IPAddress(10,0,1,99), IPAddress(10,0,1,1), IPAddress(255,255,255,0));
  
  //add all your parameters here
 // wifiManager.addParameter(&custom_mqtt_server);
 // wifiManager.addParameter(&custom_mqtt_port);
  wifiManager.addParameter(&custom_blynk_token);

  //reset settings - for testing
 wifiManager.resetSettings();

  //set minimu quality of signal so it ignores AP's under that quality
  //defaults to 8%
  //wifiManager.setMinimumSignalQuality();
  
  //sets timeout until configuration portal gets turned off
  //useful to make it all retry or go to sleep
  //in seconds
  //wifiManager.setTimeout(120);

  //fetches ssid and pass and tries to connect
  //if it does not connect it starts an access point with the specified name
  //here  "AutoConnectAP"
  //and goes into a blocking loop awaiting configuration
  if (!wifiManager.autoConnect()) {
    Serial.println("failed to connect and hit timeout");
    delay(3000);
    //reset and try again, or maybe put it to deep sleep
    ESP.reset();
    delay(5000);
  }

  //if you get here you have connected to the WiFi
  Serial.println("connected...yeey :)");

  //read updated parameters
//  strcpy(mqtt_server, custom_mqtt_server.getValue());
//  strcpy(mqtt_port, custom_mqtt_port.getValue());
  strcpy(blynk_token, custom_blynk_token.getValue());
Serial.println(blynk_token);
  //save the custom parameters to FS
  if (shouldSaveConfig) {
    Serial.println("saving config");
    DynamicJsonBuffer jsonBuffer;
    JsonObject& json = jsonBuffer.createObject();
 //   json["mqtt_server"] = mqtt_server;
 //   json["mqtt_port"] = mqtt_port;
    json["blynk_token"] = blynk_token;

    File configFile = SPIFFS.open("/config.json", "w");
    if (!configFile) {
      Serial.println("failed to open config file for writing");
    }

    json.printTo(Serial);
    json.printTo(configFile);
    configFile.close();
    //end save
  }

  Serial.println("local ip");
  Serial.println(WiFi.localIP());

Blynk.config(blynk_token);
  Blynk.connect();
  timer.setInterval(1000L,myTimerEvent);
 //my setup{
 
  

}



void loop()
{
  Blynk.run(); 
  timer.run();
   
}
















